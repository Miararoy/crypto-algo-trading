# Crypto Algo Trading

## General

This implementation has 4 parts
1. Get the data
2. Create new Dataset combining the taken action, parameters and result
3. Find the best Parameters
4. Test on data

The approach taken was to re-generate new dataset and from it, to infer the optimal solution.
The re-generating process is as follows:
- select random lagging window size
- select random leading window size
- select random threshold values
- randomly choose a time-window
- apply SMA by lagging window size & leading window size
- apply action according to strategy
- calculate the amount of btc gained

Note: I've also calculated the normalized "total_btc" which is the total_btc
divided by the difference of the min and max values in the window (was the window volatile)
this approach however had failed to execute better.

about calculating the total BTC gained:
We start with 1.0 BTC
Then, we have 3 possible actions - BUY, SELL, HOLD
- BUY: use all the BTC to buy LTC
- SELL: sell all the LTC and get BTC
- HOLD: do nothing

## Running scripts

clone the project, on project base dir:

#### Get the data

run ```python3 src/save_data.py```

this will save a .parquet file with all the data from the API

#### Create new dataset

run ```python3 src/generate_data_for_learning.py```

this will run 10 ** 4 iterations of 10 ** 4 raw data points

#### Find best parameters

here's the tricky part. while wanted to implement 3 approaches only 2 could be implemented.

run ```python3 src/find_max_values.py```

this will calculate 2 parameter selection
1. Naive: Select the values of the max "total_btc"
results:
```
Max value naive: 
{
    'lagging_window_size': 2112,
    'leading_window_size': 502,
    'threshold': 0.043300000000000005
}
```

these parameters running on all data would gain 790% on investment


2. Ensemble: Select the mean value of the best 100 rows by "total_btc"
results:
```
Max ensemble value: 
lagg_w_size        3753.470000
lead_w_size        1203.930000
threshold             0.046943
total_btc             2.504230
```

these parameters would gain only 102% (!) which is indeed an interesting result.

#### Test on all the data

src/test_best_configuration.py enables us to run a calculation of total_btc gain on a time interval
```
python3 src/test_best_configuration.py 

usage: test_best_configuration.py [-h] [--from_date FROM_DATE]
                                  [--to_date TO_DATE]
                                  [--lagging_window_size LAGGING_WINDOW_SIZE]
                                  [--leading_window_size LEADING_WINDOW_SIZE]
                                  [--threshold THRESHOLD]

optional arguments:
  -h, --help            show this help message and exit
  --from_date FROM_DATE
                        Test parameters from date
  --to_date TO_DATE     Test parameters to date
  --lagging_window_size LAGGING_WINDOW_SIZE
                        Test parameters to date
  --leading_window_size LEADING_WINDOW_SIZE
                        Test parameters to date
  --threshold THRESHOLD
                        Test parameters to date
```

## Questions:

- What was the BTC-LTC return since 1/1/2018? What was your model’s performance during the same period?

running 
```
python3 src/test_best_configuration.py --from_date '2018-01-01 00:00:00' --lagging_window_size 2112 --leading_window_size 502 --threshold 0.043300000000000005
```
 results in: 0.7378835625374156 total BTC
 while you could profit ±1.5 during this time.

- suggest a more complex algorithm that is
suitable for this kind of problems. Why do you think it might perform better?

i have tried to implement a stochastic oscillator
this approach, more in use in real world, give an indication on over-buying or over-selling but did not perform well when manually testing it on crypto. i think the indications i used ought to be more rapid, since the crypto market is very volatile. i believe stochastic-oscillator would perform better because it has a better indication to using moving average. instead of using long-short window it uses a slow-fast window which is generally a better indication to market behavior (this is also what i would tune better "rapidity")

- Model robustness and new data

there is a lot of randomality in this implementation, the time-window is selected randomly so that for each iteration we select a new timeline, thus increasing robustness by ensuring we avoid local-effects.
with more time in our hands we could select a random time-window size and run ± 10 ** 6 iteration (100 for each set of parameters on a different window)

we could test our model on new data by running the performing a linear regression (implemented on src/linear_regression.py) - try predicting the gain, and then testing against the reality (MSE for example).

** regarding the rest of the questions, ive skipped some test to hand over this work that has taken well over 10 hours **






