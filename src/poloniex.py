from exchange import Exchange
import matplotlib.pyplot as plt


class Poloniex(Exchange):
    """
    exchange implementation for Poloniex https://poloniex.com/
    """
    def __init__(self):
        Exchange.__init__(self, "https://poloniex.com/public")
