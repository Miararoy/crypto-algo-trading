import pandas as pd


def stochastic_oscillator(df, n_k, n_d):
    def validate_h_l_c(df):
        return ("high" in df.columns and
                "low" in df.columns and
                "close" in df.columns)

    def sto_k(df):
        # mone = (df["close"] - df["close"].rolling(n_k).min())
        # mehane = (df["close"].rolling(n_k).min() -
        #           df["close"].rolling(n_k).max())
        # df["%K"] = (mone / mehane) * 100
        print(df.head(5).to_string())
        df["%K"] = ((df["close"] - df["close"].rolling(14).min()) / (df["close"].rolling(14).max() - df["close"].rolling(14).min())) * 100
        print(df.head(5).to_string())
        return df
    
    def sto_d(df):
        df["%D"] = df["%K"].rolling(n_d).mean()
        return df
    
    if validate_h_l_c(df):
        out_df = sto_d(sto_k(df))
        print(out_df.head(5).to_string())
        return out_df
    else:
        raise ValueError(
            "df must have high, low and close columns" +
            " current columns are {}".format(list(df.columns))
        )


class StochasticOscillator(object):
    """
    class that implement stochastic ocsillator indicator
    args:
        df: dataframe
        n_k: moving window size for raw price
        n_d: moving avg. for n_k avg.
    """
    def __init__(self, n_k=14, n_d=3):
        self._n_k = n_k
        self._n_d = n_d
    
    def apply(self, df):
        """
        Applies stochastic ocsillator to data frame
        add %K, %D columns
        """
        return stochastic_oscillator(
            df,
            self._n_k,
            self._n_d
        )
