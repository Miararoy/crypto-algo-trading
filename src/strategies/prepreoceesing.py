class Processing(object):
    """
    base class for preparing data prior to 
    applying strategy
    """
    def __init__(self, process, **params):
        self._process = process(**params)

    def apply(self, data):
        return self._process(data)
