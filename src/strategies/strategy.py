from .prepreoceesing import Processing


class Strategy(Processing):
    """
    base class for applying strategy given paramets
    """
    def __init__(self, strategy, **params):
        Processing.__init__(self, strategy, **params)


class Process(Processing):
    def __init__(self, data, process, **params):
        Processing.__init__(self, process, **params)
        self.processed_data = self.apply(data)
    
    def get(self):
        return self.processed_data