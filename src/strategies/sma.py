from .strategy import Strategy, Process


def calculate_action(close, lagg, lead, threshold):
    if lead - lagg > threshold * close:
        return 1
    elif lagg - lead > threshold * close:
        return -1
    else:
        return 0


def simple_strategy(thresh):
    def do_simple_strategy(df):
        df["action"] = df.apply(
            lambda row: calculate_action(
                row["close"],
                row["lagg"],
                row["lead"],
                thresh
            ),
            axis=1
        )
        return df
    return do_simple_strategy


def simple_strategy_processing(
    lagging_window_size,
    leading_window_size
):
    def procssing(df):
        df["lagg"] = df.close.rolling(
            lagging_window_size,
            # min_periods=1
        ).mean()
        df["lead"] = df.close.rolling(
            leading_window_size,
            # min_periods=1
        ).mean()
        df = df.dropna()
        return df
    
    return procssing
    

class SimpleStrategySMA(Strategy):
    def __init__(self, threshold):
        Strategy.__init__(self, simple_strategy, thresh=threshold)


class SimpleStrategyDataProcessing(Process):
    def __init__(self, data, lagging_window_size, leading_window_size):
        Process.__init__(
            self,
            data,
            simple_strategy_processing,
            lagging_window_size=lagging_window_size,
            leading_window_size=leading_window_size
        )
        