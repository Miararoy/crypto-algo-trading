from strategies.sma import SimpleStrategySMA, SimpleStrategyDataProcessing
from score.bank import Bank


def run_data_iteration(
    df,
    start_index,
    end_index,
    lagging_window_size,
    leading_window_size,
    threshold
):
    if start_index > 0 and end_index > 0:
        sample = df.iloc[start_index:end_index]
    else:
        sample = df
    processed_data = SimpleStrategyDataProcessing(
        sample,
        lagging_window_size=lagging_window_size,
        leading_window_size=leading_window_size,
    ).get()
    data_with_action = SimpleStrategySMA(threshold).apply(
        processed_data
    )
    return Bank().calc_total_earnings(data_with_action)
