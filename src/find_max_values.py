import pandas as pd
import numpy as np
import scipy.stats as stats
from os.path import isfile, join
from os import listdir
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

BASE_DATA_FOLDER = "output"


def load_data(base_folder):
    return pd.read_csv(
        join(
            base_folder,
            max([f for f in listdir(base_folder) if isfile(join(base_folder, f))])
        )
    )


def plot_4D_data(df, features, result):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    if len(features) != 3:
        raise ValueError("use 1-3 features")
    else:
        ax.scatter(
            df[[features[0]]].values,
            df[[features[1]]].values,
            df[[features[2]]].values,
            c=df[[result]].values,
            cmap=plt.hot()
        )
    plt.show()


def find_max_avg_top(df, col, N):
    return df.nlargest(N, col).mean().to_string()
 

def find_max_naive(df, col):
    best = df.loc[df[col].idxmax()]
    max_vals = {
        "lagging_window_size": int(best.lagg_w_size),
        "leading_window_size": int(best.lead_w_size), 
        "threshold": best.threshold
    }
    return max_vals


print("Max value naive: {}".format(find_max_naive(
    load_data(BASE_DATA_FOLDER),
    "total_btc"
)))


print("Max ensemble value: {}".format(find_max_avg_top(
    load_data(BASE_DATA_FOLDER),
    "total_btc",
    100
)))