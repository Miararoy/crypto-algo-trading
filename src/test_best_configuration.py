import pandas as pd
from prepare_data import run_data_iteration
import argparse

parser = argparse.ArgumentParser()
parser.add_argument(
    "--from_date",
    help="Test parameters from date"
)
parser.add_argument(
    "--to_date",
    help="Test parameters to date"
)
parser.add_argument(
    "--lagging_window_size",
    help="Test parameters to date",
    type=int
)
parser.add_argument(
    "--leading_window_size",
    help="Test parameters to date",
    type=int
)
parser.add_argument(
    "--threshold",
    help="Test parameters to date",
    type=float
)


def main():
    args = parser.parse_args()

    df = pd.read_parquet(
        "./poloniex_btc_ltc_300s_all_data.parquet"
    )
    if args.from_date:
        df = df.loc[df.index > args.from_date]
    if args.to_date:
        df = df.loc[df.index < args.to_date]
    lagg = args.lagging_window_size or 5000
    lead = args.leading_window_size or 1000
    thresh = args.threshold or 0.025

    total_btc, norm_total_btc = run_data_iteration(
        df,
        -1,
        -1,
        lagg,
        lead,
        thresh
    )
    print(total_btc)


if __name__ == "__main__":
    main()