from poloniex import Poloniex

PARAMS = {
    "command": "returnChartData",
    "currencyPair": "BTC_LTC",
    "start": 0,
    "end": 9999999999,
    "period": 300,
}

p = Poloniex()
p.get_data(
    query_params=PARAMS
)
df = p.to_pandas()
df = df[["close", "date"]]
df = df.set_index("date")

df.to_parquet("./poloniex_btc_ltc_300s_all_data.parquet")