import pandas as pd
from time import time
import os
import random
import numpy as np
from prepare_data import run_data_iteration

OUTPUT_FOLDER = "output"
LINES_PER_BATCH = 10 ** 4
NUM_LINES = 10 ** 4

def main():
    leading_window_values = [
        int(x) for (x) in list(np.linspace(500, 2000, 1501))
    ]
    lagging_window_values = [
        int(x) for (x) in list(np.linspace(2000, 6000, 1501))
    ]
    threshold_values = list(np.linspace(0.01, 0.1, 1501))

    raw_data = pd.read_parquet(
        "./poloniex_btc_ltc_300s_all_data.parquet"
    )
    
    total_lines = len(raw_data.index)

    df = pd.DataFrame(
        columns=[
            "data_size",
            "lagg_w_size",
            "lead_w_size",
            "threshold",
            "total_btc",
            "norm_total_btc"
        ]
    )

    for i in range(1, NUM_LINES):
        min_index = random.randint(1, total_lines - LINES_PER_BATCH)
        max_index = min_index + LINES_PER_BATCH
        lagg = random.choice(lagging_window_values)
        lead = random.choice(leading_window_values)
        thresh = random.choice(threshold_values)
        total_btc, norm_total_btc = run_data_iteration(
            raw_data,
            min_index,
            max_index,
            lagg,
            lead,
            thresh
        )
        df = df.append(
            {
                "data_size": LINES_PER_BATCH,
                "lagg_w_size": lagg,
                "lead_w_size": lead,
                "threshold": thresh,
                "total_btc": total_btc,
                "norm_total_btc": norm_total_btc
            },
            ignore_index=True
        )
        print("Generating Data: {}%".format(round((100 * i) / NUM_LINES), 2))
    output_file = os.path.join(OUTPUT_FOLDER, str(int(time())) + ".csv")
    df.to_csv(output_file)
    print("Data Saved: {}".format(output_file))


if __name__ == "__main__":
    main()