import requests
import json
import pandas as pd


def get_data_json(url: str, params: dict):
    """
    returns closing price data from 
    """
    response = requests.get(
        url,
        params
    )
    if response.ok:
        try:
            load = response.json()
            return load
        except ValueError:
            raise ValueError(
                "Response returned with status code {}".format(
                    response.status_code
                ) +
                " but data = {} ; is not json parsable".format(
                    response.text
                )
            )
    else:
        raise Exception(
            "request failed and " +
            "Response returned with status code {}".format(
                response.status_code
            )
        )


class Exchange(object):
    """
    implements integration to exchange
    """
    def __init__(self, exchange_base_path: str):
        """
        initializes exchange with base path
        args: 
            exchange_base_path(str): base path for getting data via api
        """
        self._base_path = exchange_base_path
        self._query_params = None
        self._data = None
    
    def get_data(self, query_params: dict):
        """
        gets data from exchange according to query parameters
        """
        self._query_params = query_params
        self._data = get_data_json(
            self._base_path,
            self._query_params
        )
        if not isinstance(self._data, list):
            raise ValueError(
                "data returned from server must be of type list" +
                " data type is {}".format(type(self._data))
            )
        return self._data
    
    def to_pandas(self, schema: list = []):
        """
        returns data as pandas dataframe according to fields in data
        args:
            schema(list): list of data fields to return
        returns:
            df(pandas.Dataframe): data frame
        """
        sample = self._data.pop()
        if not isinstance(sample, dict):
            raise ValueError(
                "data entries from server must be of type dict"
            )
        if schema:
            for field in schema:
                if field not in sample.keys():
                    raise AssertionError(
                        "schema key {k} is not in data keys {keys}".format(
                            k=field,
                            keys=sample.keys()
                        )
                    )
        df = pd.read_json(
            json.dumps(
                self._data
            )
        )
        if not schema:
            return df
        else:
            return df[schema]


if __name__ == "__main__":
    poloniex = Exchange(
        return_chart_data_base_path
    )
    poloniex.get_data(
        params
    )
    df = poloniex.to_pandas()
    print(df.count())
    print(df.head(5).to_string())