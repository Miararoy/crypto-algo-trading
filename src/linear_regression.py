from sklearn import datasets, linear_model, preprocessing
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error, r2_score
from find_max_values import load_data
import matplotlib.pyplot as plt

DATA_BASE_FOLDER = "output"


def main():
    # min_max_scaler = preprocessing.MinMaxScaler()
    data = load_data(DATA_BASE_FOLDER)[
        ["lagg_w_size", "lead_w_size", "threshold", "total_btc"]
    ]
    print(data.head(10).to_string())
    x, x_test, y, y_test = train_test_split(
        data.drop(["total_btc"], axis=1),
        data["total_btc"],
        train_size=0.2
    )
    regr = linear_model.LinearRegression()
    regr.fit(x, y)
    prediction_results = regr.predict(x_test)
    print("MSE = {}".format(mean_squared_error(y_test, prediction_results)))
    print("COEF = {}".format(regr.coef_))
    print("INDI = {}".format(regr.intercept_))


if __name__ == "__main__":
    main()