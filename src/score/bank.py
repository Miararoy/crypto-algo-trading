class Bank(object):
    def __init__(self):
        self.btc = 1
        self.ltc = 0
    
    def calc_total_earnings(self, df):
        min_close_btc = 1 / df.close.max()
        max_close_btc = 1 / df.close.min()
        diff_close = abs(max_close_btc - min_close_btc)
        for _, row in df.iterrows():
            if row['action'] == 1:
                # BUY
                self.ltc = self.ltc + self.btc * (1 / row['close'])
                self.btc = 0
            elif row['action'] == -1:
                # SHORT
                self.btc = self.btc + self.ltc * row['close']
                self.ltc = 0
            else:
                # HOLD
                pass
        self.btc = self.btc + self.ltc * df.iloc[-1]["close"]
        return self.btc, self.btc / diff_close